package var;

public class Var {
    private int number;

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
       String s = "var = " + number;
        return s;
    }
}
